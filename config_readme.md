# General recommendation  
It fully up to you to decide later on, IF and how you will configure a re-sampling.  
Regarding what affect the most the audio quality  
My findings after long listening hours are in order:  
- The quality or the recording itself (you can't do anything about it)  
- The file audio format, lossless recommended !  
- your headphone/speakers  
  
The rest will never affect your listening experience enough to the point that you even realize it.  
Therefore each time it's possible, I force my output/hardware to: **24bit 96KHz** which always gave me perffect results, and allow any other games/applications to run audio with foobar2000 playing in the background.  
  
For optimal audio quality (in audio only mode), in foobar2000 configuration, set your audio device ideally in **ASIO** mode, or Exclusive.  
  
# Downloads  
### Get foobar2000 for windows and install it  
https://www.foobar2000.org/download  
   
### Standard Plugins I'm currently using (get update notification)  
https://www.foobar2000.org/components  
- ABX Comparator  
- AC3 decoder  
- Album List Panel  
- ASIO Output  
- Audio CD Writer  
- Binary Comparator  
- BPM Analyser  
- Chacon 3  
- Command-Line Decoder Wrapper  
- Decoding Speed Test  
- Dolby Headphone Wrapper
- Discogs Tagger  
- Dynamic Decoder Info  
- EBU R128 Normalizer  
- Facets  
- Fade In/Out DSP
- FFmpeg Decoder Wrapper  
- File Integrity Verifier  
- Graphic Equalizer  
- HDCD decoder  
- Impulse Response Convolver  
- Keep Queue  
- Kernel Streaming support  
- Lyric Show Panel  
- Masstagger  
- MathAudio Headphone  
- Meier Crossfeed  
- Monkey's Audio Decoder  
- MultiResampler  
- Oscilloscope (Direct2D)  
- Playback Statistics  
- Playlist History  
- Playlist Organizer  
- Playlists Dropdown  
- Quick Search Toolbar  
- projectM - The most advanced open-source music visualizer  
- RAM-Disk  
- Random Pools
- Run Services  
- UPnP MediaRenderer Output  
- UPnP/DLNA Renderer, Server, Control Point  
- (v1.6.7+ not needed anymore) WASAPI output support  
- (v1.6.7+ not needed anymore) WASAPI shared output   
- VST 2.x/3.x Adapter
- Waveform Seekbar  
- Waveform Minibar (mod)  
- Youtube Source
  
### (optional) Side Plugins (need to manually check for updates)  
- Free Encoders Pack: [download](https://www.foobar2000.org/encoderpack) (uncompress encoders into foobar2000 directory)  
- Musical Spectrum: [download](https://wiki.hydrogenaud.io/index.php?title=Foobar2000:Components/Musical_Spectrum_(foo_musical_spectrum))  
- Analog VU Meters [download](https://audio-file.org/2015/11/07/foobar-default-user-interface-installing-analog-vu-meters/)  
- [BS2B (foo dsp bs2b)](https://wiki.hydrogenaud.io/index.php?title=Foobar2000:Components/BS2B_(foo_dsp_bs2b)): [download](https://bs2b.sourceforge.net/download.html)
  
### (optional) [Third-party binaries](https://fy.3dyd.com/help/third_party_binaries/) (need to manually check for updates)  
- ffmpeg: [download](https://github.com/BtbN/FFmpeg-Builds/releases) (ffmpeg main: https://ffmpeg.org/)  
- LAV filters: [download](https://github.com/Nevcairiel/LAVFilters/releases), (git repo: https://github.com/Nevcairiel/LAVFilters/)  
- libcurl: [download](https://curl.se/download.html)  
- madVR: [download](https://madshi.net/madVR.zip) (madVR home: https://madvr.com/)  
- xy-VSFilter: [download](https://github.com/Cyberbeing/xy-VSFilter/releases),  
- youtube-dl: [download](https://ytdl-org.github.io/youtube-dl/download.html), (git repo: https://github.com/ytdl-org/youtube-dl)  
Once third-party is correctly configured, it should look like this:  ![plot](pic/foobar2000-third-party.png)
  
### DSD/SACD/Upsampler plugins  
- SoX Resampler: [download](https://hydrogenaud.io/index.php?topic=67373.0)  
- SSRC|PPHS resampler: [download](https://wiki.hydrogenaud.io/index.php?title=Foobar2000:Components/SSRC_(foo_dsp_ssrc))  
Install foo_dsp_ssrcX.dll, not foo_dsp_ssrc_highprec.dll  
- Super Audio CD Decoder & DSD Processor  
foo_input_sacd, foo_out_asio and foo_out_asio+dsd from: [download](https://sourceforge.net/projects/sacddecoder/files/)  
foo_out_asio%2Bdsd/)  
- [VST adapter](https://wiki.hydrogenaud.io/index.php?title=Foobar2000:Components/VST_adapter)  
   
# Configuration (open preferences)  
**Display -> Default User Interface** Update the: Status bar with the following line, to get elapsed and remaining time when playing.  
```
%playback_time% | $ifgreater(%length_seconds%,0,%length%,continuous)[ | %playback_time_remaining% ] | %codec% | %channels% | %samplerate% Hz | %bitrate% kbps  
```  
Once done, right click on the status bar, and select:  
- Show total duration of selected tracks  
- Show count of selected tracks  
  
**Display -> Default User Interface -> Playlist View ->** create new custom columns  
```
RG-track    |  %replaygain_track_gain%
T-Peakmeter |  %replaygain_track_peak%
Date_Added  |  %last_modified%
T-Peak_db   |  %replaygain_track_peak_db%
Sample Rate |  %samplerate%
```  
   
**Display -> Keyboard Shortcuts ->** add global shortcuts  
```
CTRL-PgUp   | previous track
CTRL-PgDown | Next Track
CTRL-Space  | Play|Pause
CTRL-Right  | Add to playback queue
CTRL-Left   | Remove from playback queue
```  

**Media Library -> Album List ->** If not existing add views  
```
by artist | %<artist>%|%album%|[[%discnumber%.]%tracknumber%. ][%track artist% - ]%title%
by album  | %album%[ '['%album artist%']']|[[%discnumber%.]%tracknumber%. ][%track artist% - ]%title%
by year   | %<date>%|[%album artist% - ]%album%|[[%discnumber%.]%tracknumber%. ][%track artist% - ]%title%
by genre  | %<genre>%|[%album artist% - ]%album%|[[%discnumber%.]%tracknumber%. ][%track artist% - ]%title%
```  
  
**Media Library -> Album List Panel ->** If not existing add views  
```
by album        | %album%[ '['%album artist%']']|[[%discnumber%.]%tracknumber%. ][%track artist% - ]%title%
by artist       | %<artist>%|%album%|[[%discnumber%.]%tracknumber%. ][%track artist% - ]%title%
by genre        | %<genre>%|[%album artist% - ]%album%|[[%discnumber%.]%tracknumber%. ][%track artist% - ]%title%
by year         | %<date>%|[%album artist% - ]%album%|[[%discnumber%.]%tracknumber%. ][%track artist% - ]%title%
by artist/album | [%album artist% - ]['['%date%']' ]%album%|[[%discnumber%.]%tracknumber%. ][%track artist% - ]%title%
```  
  
**Playback ->**: source mode: track, apply gain and prevent clipping according to peak, Pream: With RG info: +3.0dB   
  
**Playback -> Decoding ->**: all selected, drag Super Audio CD Decoder to the top of the list  
  
**Playback -> DSP Manager ->**: Active Downstream channels to stereo, and (optionaly) Advanced Limiter  
  
**optional DSP Manager config (PCM over i2s/USB/Coax/Optical)** resampling with SSRC X via DSP Manager.  
SSRC X settings, depends on your DAC, I select only the highest sample rate supported for 44.1KHz and 48KHz variants.  
![plot](pic/SSRCX.png)  
i.e. :  
**88200** and **96000** with Preset **24bit low**  
**176400** and **192000** with Preset **24bit low**  
(Preset 24bit low create a little bit of echo, a longer decay)  
For each config about save them respectivly as oversampling_up_to_96000 and oversampling_up_to_192000  
Then remove the SSRC X DSP, and save a new config as Non_Over_Sampling  
  
**Configure Buttons for switching (Non) Over Sampling Modes**  
Right click on the **Toolbar -> Customize buttons...**  
Select from Available Commands: **Main -> Playback -> DSP settings -> Non_Over_Sampling**  
Select from Available Commands: **Main -> Playback -> DSP settings -> oversampling_up_to_96000**  
Select from Available Commands: **Main -> Playback -> DSP settings -> oversampling_up_to_192000**  
Icon now appear on the left side, and on the toolbar, from the left side, you may change the icon for each of them  
  
**!!** Upsampling will be taken in account only when using ASIO drivers or WASAPI exclusive mode  
  
  
**optional DSP (DSD over USB)** via DSP Processor from the Tools section.  
select enable DSP Processor.  
As output select the best output capability of your DAC, example for 44100 -> DSD512, for 48000 -> DSD/512/48 ...  
As SDM Type chose your favorite (if you hear any diff), I used SDM type: B, Window: Hann, Sample&Hold: None, Precision: fp64  
![plot](pic/DSPProcessor.png)  
  
**Playback -> Output ->**: Wasapi push, or ASIO for better results  
Wasapi shared if want to play music and have sound mixed with system (sample rate limited, quality is lower)  
Buffer lengh, here lower is better but setting it too low can cause issue depending on your device: 50ms if possible  
output data format (depends on your DAC): 24bit or 32bit  
If ASIO or (ASIO+DSD) (recommended for RME ADI2-FS DAC), Active 64bit drivers, high priority, and reload driver  
  
**Playback -> Tools -> SACD ->**: (again, depending on your DAC) DSP+PCM, DoP for Converter, PCM samplerate: 352800, DSD2PCM Mode: Direct 64fp, 30Hz lowpass, Prefered Area stereo, DSP Processor enabled.  
  
**Advanced -> Decoding ->**: Tone/Sweep sample rate: 96000 | 192000  
  
**Advanced -> Display -> Album Art ->** Embedded vs external: Prefer embedded  
  
**Advanced -> Playback -> WASAPI**: High worker process priority: enabled  
  
**Advanced -> Playback -> WASAPI (shared)**: Resampler quality: 100%  
   
**Advanced -> Playback -> Thread priority**: Thread priority: 7  
  
**Advanced -> Playback -> Buffering**: Full file buffering up to: 10000  
  
**Advanced -> Playback -> Thread priority**: MMCSS mode (type in):Pro Audio  
  
**Advanced -> Tagging -> MP3 -> Tag writing scheme for untagged files**: ID3v2  
  
**Advanced -> Tagging -> Opus -> Header gain**: Use Track Gain  
  
**Advanced -> Tagging -> SoundCheck -> Automatically write when writing ReplayGain...**: Write Track Gain  
  
**Advanced -> Tagging -> SoundCheck -> SoundCheck target loudness**: iTunes (-16dB LUFS)  
  
# Converter  
Right-click the relevant file(s) in a playlist to bring up the context menu.  
Select "Convert" and then "..." to load the Converter Setup window.  
   
### Configure the ALAC (Apple Lossless) converter  
**Output format**  
  - Name: Apple Lossless  
  - Ouput bit depth: Auto  
  - Dither: never  

**Destination**  
  - Output path: Source track folder  
  - If file already exists: Ask  
  - Output style and file name format  
    - Convert each track to an individual file  
      - Name format: `%artist% - %title%`
  
**Processing**
  - Additional decoding: enable  
  
**Other**
  - Preview generation: disabled
  When done:
    - Transfer metadata (tags): enabled  
    - Transfer ReplayGain info: enabled  
    - Transfer attached pictures: enabled  
  
